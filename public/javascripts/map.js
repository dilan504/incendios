var map = L.map('main_map').setView([-17.2871349, -66.3932875], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var polygon = L.polygon([
    [-17.2471349, -66.3532875],//punto arriba derecha
    [-17.3271349, -66.3532875],//punto abajo derecha
    [-17.3271349, -66.4332875],//punto abajo izquierda
    [-17.2471349, -66.4332875]
]).addTo(map);

var marker1 = L.marker([-17.2471349, -66.3532875]).addTo(map);
var marker2 = L.marker([-17.3271349, -66.3532875]).addTo(map);
var marker3 = L.marker([-17.3271349, -66.4332875]).addTo(map);
var marker4 = L.marker([-17.2471349, -66.4332875]).addTo(map);