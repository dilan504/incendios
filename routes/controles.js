var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.render('controles/index');
});
router.get('/alarmas', function(req, res) {
    res.render('controles/alarmas/alarmas');
});
router.get('/patrullar', function(req, res) {
    res.render('controles/patrullar/index');
});

module.exports = router;